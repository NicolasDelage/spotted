package com.spotted;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.spotted.actors.Light;
import com.spotted.actors.Player;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GameModel {
    private ArrayList<Player> players;
    private Light light;
    private Skin playerSkin = new Skin(Gdx.files.internal("skin/arcade/arcade-ui.json"));
    private Map<String, Texture> textures;

    public GameModel() {
        this.players = new ArrayList<Player>();
        this.light = new Light(false, false,69, 0, 0, 2000);
        textures = new HashMap<String, Texture>();
        textures.put("RED", new Texture("sprite/RED.png"));
        textures.put("BLUE", new Texture("sprite/BLUE.png"));
        textures.put("YELLOW", new Texture("sprite/YELLOW.png"));
        textures.put("GREEN", new Texture("sprite/GREEN.png"));
        textures.put("PURPLE", new Texture("sprite/PURPLE.png"));
        textures.put("WHITE", new Texture("sprite/WHITE.png"));
        textures.put("GREY", new Texture("sprite/GREY.png"));
        textures.put("PINK", new Texture("sprite/PINK.png"));
        textures.put("BROWN", new Texture("sprite/BROWN.png"));
        textures.put("ORANGE", new Texture("sprite/ORANGE.png"));
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    public Texture getTextureByColor(String color) {
        return textures.get(color);
    }


    public Skin getPlayerSkin() {
        return playerSkin;
    }

    public void setPlayerSkin(Skin playerSkin) {
        this.playerSkin = playerSkin;
    }
}
