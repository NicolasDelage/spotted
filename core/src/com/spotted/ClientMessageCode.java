package com.spotted;

public enum ClientMessageCode {
    JOIN ("100: JOIN"),
    START_GAME ("101: START GAME"),
    MOVE_REQUEST ("102: MOVE REQUEST");

    private String messsage;

    ClientMessageCode(String messsage) {
        this.messsage = messsage;
    }

    public String getMessage() {
        return messsage;
    }
}