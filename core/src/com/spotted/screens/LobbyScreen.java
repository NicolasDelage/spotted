package com.spotted.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.spotted.GameModel;
import com.spotted.MySpottedGame;
import com.spotted.SpottedClient;
import com.spotted.actors.Player;

import javax.xml.crypto.Data;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class LobbyScreen implements Screen {
    private Stage stage;
    private MySpottedGame game;
    private GameModel gameModel;
    private SpottedClient spottedClient;

    public LobbyScreen(MySpottedGame game, GameModel gameModel, SpottedClient spottedClient){
        this.game = game;
        this.gameModel= gameModel;
        this.spottedClient = spottedClient;
    }

    private void startGame(){
        game.setScreen(new GameScreen(game, gameModel, spottedClient));
        spottedClient.startGame();
    }

    @Override
    public void show() {
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);

        Skin skin = new Skin(Gdx.files.internal("skin/arcade/arcade-ui.json"));

        Label waitingLabel = new Label("WAITING FOR HOST ...", skin);
        waitingLabel.setAlignment(Align.center);
        waitingLabel.setPosition(Gdx.graphics.getWidth()/2 - waitingLabel.getWidth()/2, Gdx.graphics.getHeight() - 50);
        stage.addActor(waitingLabel);

        Button startGameButton = new Button(skin);
        startGameButton.setPosition(Gdx.graphics.getWidth() - 165, 150);
        stage.addActor(startGameButton);
        startGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y){
                startGame();
            }
        });

        Label startGameLabel = new Label("START GAME", skin);
        startGameLabel.setAlignment(Align.center);
        startGameLabel.setPosition(Gdx.graphics.getWidth() - 225, 100);
        stage.addActor(startGameLabel);

        Table hostTable = new Table();

        Label hostIpLabel = new Label("HOST IP : ", skin);
        hostIpLabel.setAlignment(Align.center);
        hostTable.add(hostIpLabel).width(100).pad(50);

        Label hostIpValue = new Label(spottedClient.getInetAddress().toString(), skin);
        hostIpValue.setAlignment(Align.center);
        hostTable.add(hostIpValue).width(300);

        hostTable.setBounds(25, 25, 500, 150);
        stage.addActor(hostTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER))
            startGame();

        Table playersTable = new Table();
        for (Player player : gameModel.getPlayers()){
            playersTable.add(player.getLabel()).width(200).pad(0, 0, 50, 20);
            playersTable.add(player.getBoardImage()).width(player.getBoardImage().getWidth()).pad(0, 0, 50, 20);
            playersTable.row();
        }
        playersTable.setBounds(Gdx.graphics.getWidth() / 2 - 250, 150,500,Gdx.graphics.getHeight() - 300);
        stage.addActor(playersTable);

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
