package com.spotted.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.spotted.*;
import com.spotted.actors.Light;
import com.spotted.actors.Player;

import java.net.DatagramSocket;

public class GameScreen implements Screen {
    private final MySpottedGame game;
    private Stage stage;
    private ShapeRenderer shapeRenderer;
    private Image gameBoardImage;
    private Light light;
    private GameModel gameModel;
    private SpottedClient spottedClient;

    public GameScreen(MySpottedGame game, GameModel gameModel, SpottedClient spottedClient) {
        this.game = game;
        this.gameModel = gameModel;
        this.light = gameModel.getLight();
        this.spottedClient = spottedClient;
    }

    @Override
    public void show() {
        ScreenViewport viewport = new ScreenViewport();
        stage = new Stage(viewport);

        gameBoardImage = new Image(new Texture("sprite/map_spotted.png"));
        gameBoardImage.setSize((float) (Gdx.graphics.getHeight() * 0.9), (float) (Gdx.graphics.getHeight() * 0.9));
        gameBoardImage.setPosition(
                Gdx.graphics.getWidth() / 2f - gameBoardImage.getWidth() / 2,
                Gdx.graphics.getHeight() / 2f - gameBoardImage.getHeight() / 2
        );
        stage.addActor(gameBoardImage);

        shapeRenderer = new ShapeRenderer();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        MoveRequest moveRequest = new MoveRequest(false, false, false, false);


        if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
            moveRequest = new MoveRequest(false, false, false, true);
        else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            moveRequest = new MoveRequest(false, true, false, false);
        else if (Gdx.input.isKeyPressed(Input.Keys.UP))
            moveRequest = new MoveRequest(true, false, false, false);
        else if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
            moveRequest = new MoveRequest(false, false, true, false);


        if (moveRequest.isLeft() || moveRequest.isRight() || moveRequest.isBottom() || moveRequest.isTop()) {
            String mRequest = "\"top\":" + moveRequest.isTop() + ",\"right\":" + moveRequest.isRight() + ",\"bottom\":" + moveRequest.isBottom() + ",\"left\":" + moveRequest.isLeft();
            spottedClient.setJsonMoveRequest(mRequest);
            spottedClient.run();
        }


        Table playersTable = new Table();
        for (Player player : gameModel.getPlayers()){
            stage.addActor(player.getImage());

            playersTable.add(player.getLabel()).width(200).pad(0, 0, 50, 20);
            playersTable.add(player.getBoardImage()).width(player.getBoardImage().getWidth()).pad(0, 0, 50, 20);
            playersTable.row();
        }
        playersTable.setBounds(50, 150,500,Gdx.graphics.getHeight() - 300);
        stage.addActor(playersTable);


        stage.act();
        stage.draw();

        if (light.isDraw()) {
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(new Color(1, 1, 0, 0.2f));
            shapeRenderer.circle(
                    light.getX() + gameBoardImage.getX() ,
                    light.getY() + gameBoardImage.getY(),
                    light.getDiameter()
            );
            shapeRenderer.end();
            Gdx.gl.glDisable(GL20.GL_BLEND);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        stage.dispose();
        shapeRenderer.dispose();
    }
}
