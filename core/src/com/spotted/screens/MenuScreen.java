package com.spotted.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.spotted.FrameListener;
import com.spotted.GameModel;
import com.spotted.MySpottedGame;
import com.spotted.SpottedClient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class MenuScreen implements Screen {
    private Stage stage;
    private final MySpottedGame game;
    private Label createLobbyButton, joinLobbyButton, optionsButton, exitButton;
    private TextField hostIpTextField;
    private DatagramSocket socket;
    private GameModel gameModel;
    private boolean dialogOpen;

    private int itemSelected = 1;
    public MenuScreen(MySpottedGame game) throws SocketException{
        this.game = game;
        this.socket = new DatagramSocket();
        this.gameModel = new GameModel();
        this.dialogOpen = false;
    }

    private void play(){
        String address = "localhost";
        if(hostIpTextField != null)
            address = hostIpTextField.getText();

        try {
            SpottedClient spottedClient = new SpottedClient(socket, address);
            spottedClient.join();
            game.setScreen(new LobbyScreen(game, gameModel, spottedClient));
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void show() {
        run();
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);

        final Skin skin = new Skin(Gdx.files.internal("skin/arcade/arcade-ui.json"));

        Table container = new Table();
        container.setBounds(Gdx.graphics.getWidth()/2 - 200, Gdx.graphics.getHeight()/2 - 200, 400, 400);

        createLobbyButton = new Label("CREATE GAME", skin);
        createLobbyButton.setAlignment(Align.center);
        createLobbyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y){
                itemSelected = 1;
                doAction();
            }
        });
        container.add(createLobbyButton).width(createLobbyButton.getWidth()).height(createLobbyButton.getHeight()).pad(20);
        container.row();

        joinLobbyButton = new Label("JOIN GAME", skin);
        joinLobbyButton.setAlignment(Align.center);
        joinLobbyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y){
                itemSelected = 2;
                doAction();
            }
        });
        container.add(joinLobbyButton).width(joinLobbyButton.getWidth()).height(joinLobbyButton.getHeight()).pad(20);
        container.row();

        optionsButton = new Label("OPTIONS", skin);
        optionsButton.setAlignment(Align.center);
        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y){
                itemSelected = 3;
                doAction();
            }
        });
        container.add(optionsButton).width(optionsButton.getWidth()).height(optionsButton.getHeight()).pad(20);
        container.row();

        exitButton = new Label("EXIT", skin);
        exitButton.setAlignment(Align.center);
        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y){
                itemSelected = 4;
                doAction();
            }
        });
        container.add(exitButton).width(exitButton.getWidth()).height(optionsButton.getHeight()).pad(20);
        container.row();

        stage.addActor(container);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if(Gdx.input.isKeyJustPressed(Input.Keys.DOWN))
            if(itemSelected < 4) itemSelected++;
        if(Gdx.input.isKeyJustPressed(Input.Keys.UP))
            if(itemSelected > 1) itemSelected--;
        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER))
            if(!dialogOpen)
                doAction();
            else
                play();


        createLobbyButton.setColor(Color.WHITE);
        joinLobbyButton.setColor(Color.WHITE);
        optionsButton.setColor(Color.WHITE);
        exitButton.setColor(Color.WHITE);
        switch (itemSelected){
            default:
                createLobbyButton.setColor(Color.RED);
                break;
            case 2:
                joinLobbyButton.setColor(Color.RED);
                break;
            case 3:
                optionsButton.setColor(Color.RED);
                break;
            case 4:
                exitButton.setColor(Color.RED);
                break;
        }

        stage.act();
        stage.draw();
    }

    private void run(){
        new Thread(new FrameListener(socket, gameModel)).start();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private void doAction() {
        switch (itemSelected){
            default:
                play();
                break;
            case 2:
                Skin dialogSkin = new Skin(Gdx.files.internal("skin/clean-crispy/clean-crispy-ui.json"));
                final Dialog dialog = new Dialog("JOIN GAME", dialogSkin) {
                    protected void result(Object object) {
                        if(object.equals(1L)){
                            //hostIpTextField.getText();
                            play();
                        }
                        else{
                            hide();
                        }
                    }
                };

                Label hostIpLabel = new Label("HOST IP : ", dialogSkin);
                hostIpLabel.setAlignment(Align.center);
                hostIpLabel.setPosition(50, 95);
                dialog.addActor(hostIpLabel);

                hostIpTextField = new TextField("", dialogSkin);
                hostIpTextField.setPosition(170, 90);
                hostIpTextField.setAlignment(Align.center);
                dialog.addActor(hostIpTextField);

                dialog.getBackground().setMinWidth(400);
                dialog.getBackground().setMinHeight(150);
                dialog.button("READY", 1L);
                dialog.button("CANCEL", 2L);
                dialogOpen = true;
                dialog.show(stage);
                break;
            case 3:
                optionsButton.setColor(Color.RED);
                break;
            case 4:
                System.exit(-1);
                break;
        }
    }
}
