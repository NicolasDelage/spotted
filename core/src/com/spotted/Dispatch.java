package com.spotted;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spotted.actors.Player;
import com.spotted.actors.Light;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Dispatch implements Runnable {

    private DatagramSocket socket;
    private DatagramPacket packet;
    private ObjectMapper mapper;
    private GameModel gameModel;

    public Dispatch(DatagramSocket socket, DatagramPacket packet, GameModel gameModel) {
        this.socket = socket;
        this.packet = packet;
        this.mapper = new ObjectMapper();
        this.gameModel = gameModel;
    }

    @Override
    public void run() {
        try {
            JsonNode jsonNode = mapper.readTree(new String(packet.getData()));
            String serverMessage = jsonNode.get("message").asText();

            if (serverMessage.equals(ServerMessageCode.GAME_FRAME.getMessage())) {
                updatePlayers(jsonNode);
                updateLight(jsonNode);
            } else if (serverMessage.equals(ServerMessageCode.OK_JOIN.getMessage())) {
                initLobby(jsonNode);
            } else if (serverMessage.equals(ServerMessageCode.PLAYER_JOINED.getMessage())) {
                playerJoinedLobby(jsonNode);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initLobby(JsonNode jsonNode) {
        JsonNode players = jsonNode.get("players");

        for (final JsonNode player : players) {
            Player p = new Player(gameModel.getTextureByColor
                    (player.get("color").asText()),
                    player.get("pseudonym").asText(),
                    this.gameModel.getPlayerSkin()
            );
            gameModel.addPlayer(p);
        }
    }

    private void playerJoinedLobby(JsonNode jsonNode) {
        JsonNode jsonPlayer = jsonNode.get("player");

        Player player = new Player(gameModel.getTextureByColor(
                jsonPlayer.get("color").asText()),
                jsonPlayer.get("pseudonym").asText(),
                this.gameModel.getPlayerSkin()
        );

        gameModel.addPlayer(player);
    }

    private void updatePlayers(JsonNode jsonNode) {
        JsonNode players = jsonNode.get("players");

        for (final JsonNode jsonPlayer : players) {
            for (Player gamePlayer : this.gameModel.getPlayers()) {
                if (gamePlayer.getLabel().getText().toString().equals(jsonPlayer.get("pseudonym").asText())) {
                    float caveWidth = (float) (Gdx.graphics.getHeight() * 0.9);
                    float caveX = Gdx.graphics.getWidth() / 2f - caveWidth / 2;
                    float caveY = Gdx.graphics.getHeight() / 2f - caveWidth / 2;

                    float newX = caveX + jsonPlayer.get("x").asInt() * caveWidth / 724;
                    float newY = caveY + jsonPlayer.get("y").asInt() * caveWidth / 698;
                    gamePlayer.setMyPosition(newX, newY);

                    if(jsonPlayer.get("isDead").asBoolean())
                        gamePlayer.kill();

                    break;
                }
            }
        }
    }

    private void updateLight(JsonNode jsonNode) {
        JsonNode spotNode = jsonNode.get("spot");
        Light light = this.gameModel.getLight();


        if (light.isDraw()) {
            if (spotNode.get("state").asText().equals("OFF")) {
                light.setDraw(false);
            }
        } else {
            if (spotNode.get("state").asText().equals("LIT")) {
                light.setDraw(true);
            } else if (spotNode.get("state").asText().equals("WILL_LIGHT")) {
                if (!light.isFlash())
                    light.flash();
            }
        }

        light.setX(spotNode.get("x").asInt() + light.getDiameter() / 2);
        light.setY(spotNode.get("y").asInt() + light.getDiameter() / 2);
        //light.setDiameter(spotNode.get("diameter").asInt());
        light.setDiameter((int) (spotNode.get("diameter").asInt() * (Gdx.graphics.getHeight() * 0.9) / 724));
        light.setFlashTime(spotNode.get("spotlightIndicatorTimeBefore").asInt());
    }
}
