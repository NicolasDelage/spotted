package com.spotted;

import com.badlogic.gdx.Game;
import com.spotted.screens.MenuScreen;

import java.net.SocketException;
import java.net.UnknownHostException;

public class MySpottedGame extends Game {
	@Override
	public void create() {
		try {
			setScreen(new MenuScreen(this));
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
}
