package com.spotted;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.*;

public class SpottedClient implements Runnable {
    private DatagramSocket socket;
    private InetAddress inetAddress;
    private String jsonMoveRequest;
    private static final int SERVER_PORT = 9876;
    private ObjectMapper mapper;

    public SpottedClient(DatagramSocket socket, String address) throws SocketException, UnknownHostException {
        this.socket = socket;
        this.inetAddress = InetAddress.getByName(address);
        this.mapper = new ObjectMapper();
    }

    public void setJsonMoveRequest(String jsonMoveRequest) {
        this.jsonMoveRequest = jsonMoveRequest;
    }

    @Override
    public void run() {
        byte[] data = ("{ \"message\": \"" + ClientMessageCode.MOVE_REQUEST.getMessage() + "\", " + jsonMoveRequest + "}").getBytes();
        packet(data);
    }

    public void join(){
        byte[] data = ("{ \"message\": \"" + ClientMessageCode.JOIN.getMessage() + "\"}").getBytes();
        packet(data);
    }

    public void startGame(){
        byte[] data = ("{ \"message\": \"" + ClientMessageCode.START_GAME.getMessage() + "\"}").getBytes();
        packet(data);
    }

    private void packet(byte[] data){
        DatagramPacket packet = new DatagramPacket(data, data.length, this.inetAddress, SERVER_PORT);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }
}