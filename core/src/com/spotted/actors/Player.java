package com.spotted.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;


public class Player {

    private Image image;
    private Image boardImage;
    private Label label;

    public Player(Texture texture, String name, Skin skin){
        this.image = new Image(texture);
        this.boardImage = new Image(texture);
        this.label = new Label(name, skin);
        label.setAlignment(Align.center);
        label.setColor(1, 1, 1, 1f);
    }

    public void setMyPosition(float x, float y) {
        image.setPosition(x, y);
    }

    public Image getImage(){
        return image;
    }

    public Image getBoardImage(){
        return boardImage;
    }

    public Label getLabel(){
        return label;
    }

    public void kill(){
        label.setColor(1, 1, 1, 0.3f);
        image.setPosition(-1, -1);
    }
}