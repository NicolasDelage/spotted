package com.spotted.actors;

import com.badlogic.gdx.Gdx;

import java.time.Instant;

public class Light {
    private boolean draw;
    private boolean flash;

    private int diameter;
    private int flashTime;
    private int x, y;

    public Light(boolean draw, boolean flash, int diameter, int x, int y, int flashTime) {
        this.draw = draw;
        this.flash = flash;
        this.diameter = diameter;
        this.x = x;
        this.y = y;
        this.flashTime = flashTime;
    }

    public void flash()
    {
        /*timeSeconds += Gdx.graphics.getRawDeltaTime();
        if(timeSeconds > period) {
            timeSeconds -= period;
            if(flash){
                draw = !draw;
                flashRepeatCounter++;
                if(flashRepeatCounter >= 4)
                    flash = false;
            }
        }*/

        this.flash = true;
        long begin = Instant.now().toEpochMilli();

        while (Instant.now().toEpochMilli() - begin < 2000) {
            if (((Instant.now().toEpochMilli() - begin) % 200) < 100) {
                this.setDraw(true);
            } else {
                this.setDraw(false);
            }
        }

        this.flash = false;
    }

    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public int getFlashTime() {
        return flashTime;
    }

    public void setFlashTime(int flashTime) {
        this.flashTime = flashTime;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isFlash() {
        return flash;
    }
}
