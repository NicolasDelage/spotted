package com.spotted;

import com.badlogic.gdx.Game;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class FrameListener implements Runnable {
    private DatagramSocket socket;
    private GameModel gameModel;

    final private static byte[] buffer = new byte[1024];

    public FrameListener(DatagramSocket socket, GameModel gameModel) {
        this.socket = socket;
        this.gameModel = gameModel;
    }

    @Override
    public void run() {
        while (true) {
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }

            new Thread(new Dispatch(socket, packet, gameModel)).start();
        }
    }
}
