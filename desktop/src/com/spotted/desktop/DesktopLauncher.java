package com.spotted.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.spotted.MySpottedGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Spotted";
		config.width = 1920;
		config.height = 1080;
		config.resizable = false;
		new LwjglApplication(new MySpottedGame(), config);
	}
}
